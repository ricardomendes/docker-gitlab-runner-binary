# Docker image with GitLab Runner binary

A Docker image with gitlab-runner binary available in \$PATH.

[![pipeline
status](https://gitlab.com/ricardomendes/docker-gitlab-runner-binary/badges/master/pipeline.svg)](https://gitlab.com/ricardomendes/docker-gitlab-runner-binary/-/commits/master)

## Instructions

1. Build the Docker image:

   ```bash
   docker build --rm -t gitlab-runner-binary .
   ```

1. Run a container and check GitLab Runner's version:

   ```bash
   docker run --rm gitlab-runner-binary /bin/sh -c 'gitlab-runner --version'
   ```

The expected output is something like:

```sh
Version:      12.9.0
Git revision: 4c96e5ad
Git branch:   12-9-stable
GO version:   go1.13.8
Built:        2020-03-20T13:01:56+0000
OS/Arch:      linux/amd64
```
