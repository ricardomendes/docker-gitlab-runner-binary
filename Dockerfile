FROM ubuntu:18.04

RUN apt-get update -qq -y \
    && apt-get install -qq -y curl \
    && curl -L -o /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/v12.9.0/binaries/gitlab-runner-linux-amd64 \
    && chmod +x /usr/local/bin/gitlab-runner
